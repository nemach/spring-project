package com.example.demo;

import java.util.HashMap;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	
	private static HashMap<String, Product> productStore = new HashMap<>();
	
	static {
		productStore.put("1", new Product("1", "honey"));
		productStore.put("2", new Product("2", "cornflakes"));
		productStore.put("3", new Product("3", "banana"));
	}
	
	@RequestMapping(value="/products")
	public ResponseEntity<Object> getProducts() {
		return new ResponseEntity<Object>(productStore.values(), HttpStatus.OK);
	}
	
	@RequestMapping(value="/productids")
	public ResponseEntity<Object> getProductList() {
		return new ResponseEntity<Object>(productStore.keySet(), HttpStatus.OK);
	}
	
	@RequestMapping(value="/products/{id}")
	public ResponseEntity<Object> getProductDetails(@PathVariable("id") String id) {
		return new ResponseEntity<Object>(productStore.get(id).getName(), HttpStatus.OK);
	}
	
	@RequestMapping(value="/products", method = RequestMethod.POST)
	public ResponseEntity<Object> createProduct(@RequestBody Product newProduct) {
		productStore.put(newProduct.getId(), newProduct);
		return new ResponseEntity<Object>("Successfully added", HttpStatus.OK);
	}
	
	@RequestMapping(value="/products/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> deleteProduct(@PathVariable("id") String id) {
		if (null != productStore.remove(id)) {
			return new ResponseEntity<Object>("Successfully removed", HttpStatus.OK);
		} 
		else {
			return new ResponseEntity<Object>("Object not found", HttpStatus.OK);			
		}
	}
	
	@RequestMapping(value="/products/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> deleteUpdate(@PathVariable("id") String id, @RequestBody Product updatedProduct) {
		if (!productStore.containsKey(id)) {
			return new ResponseEntity<Object>("Object not found", HttpStatus.PRECONDITION_FAILED);			
		}

		updatedProduct.setId(id);
		productStore.put(id, updatedProduct);
		return new ResponseEntity<Object>("Successfully updated", HttpStatus.OK);
	}
	
	
	
}
